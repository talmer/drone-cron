FROM alpine
RUN apk add --update ca-certificates

ADD drone-cron /usr/bin/
VOLUME [ "/data" ]
ENTRYPOINT ["/usr/bin/drone-cron"]

