package main

import (
	"context"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"

	"github.com/drone/drone-go/drone"
	"github.com/ghodss/yaml"
	"github.com/robfig/cron"
	"golang.org/x/oauth2"
)

type (
	ConfigDrone struct {
		Token  string `json:"token"`
		Server string `json:"server"`
	}

	ConfigJobs struct {
		Drone ConfigDrone `json:"drone"`
		Jobs  []ConfigJob `json:"jobs"`
	}

	ConfigJob struct {
		Name     string            `json:"name"`
		Branch   string            `json:"branch"`
		Schedule string            `json:"schedule"`
		Params   map[string]string `json:"params"`
	}
)

const (
	DRONE_CRON_CONFIG   = "DRONE_CRON_CONFIG"
	DEFAULT_CONFIG_PATH = "/data/config.yaml"
)

func main() {
	// get config path

	configPath := os.Getenv(DRONE_CRON_CONFIG)
	if configPath == "" {
		configPath = DEFAULT_CONFIG_PATH
	}

	// Reading & unmarshalling the config

	if !FileExists(configPath) {
		log.Fatalf("Config file %s doesn't exists", configPath)
	}
	config, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatalf("Error reading config file %v ", err)
	}

	var jobs ConfigJobs
	if err := yaml.Unmarshal(config, &jobs); err != nil {
		log.Fatalf("Config Unmarshal error %v", err)
	}
	if jobs.Drone.Server == "" {
		log.Fatalf("Set %s which is currently empty", "[drone: server]")
	}
	jobs.Drone.Server = strings.TrimSuffix(jobs.Drone.Server, "/")

	if jobs.Drone.Token == "" {
		log.Fatalf("Set %s which is currently empty", "[drone: token]")
	}

	ctx, cancel := context.WithCancel(context.Background())

	// Setting up a client to talk to drone with

	httpClient := oauth2.NewClient(ctx, oauth2.StaticTokenSource(&oauth2.Token{AccessToken: jobs.Drone.Token}))
	client := drone.NewClient(jobs.Drone.Server, httpClient)

	_, err = client.Self()
	if err != nil {
		log.Fatalf("Failed to ping drone: %v\n", err)
	}

	cs := CronScheduler{client: client}

	// Setting up and running Cron

	c := cron.New()

	log.Println("Adding jobs with their schedule:")
	for _, job := range jobs.Jobs {
		log.Println(job.Schedule, job.Name)
		c.AddFunc(job.Schedule, cs.BuildStart(job.Name, job.Branch, job.Params))
	}

	go c.Run()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt)

	<-sig
	log.Println("Received an interrupt signal, shutting down")
	c.Stop()
	cancel()
}

type CronScheduler struct {
	client drone.Client
}

func (cs *CronScheduler) BuildStart(repo, branch string, params map[string]string) cron.FuncJob {
	splits := strings.Split(repo, "/")
	if len(splits) != 2 {
		log.Println("Failed to split repo name:", repo)
	}
	owner, name := splits[0], splits[1]

	return func() {
		if branch == "" {
			branch = "master"
		}

		lastBuild, err := cs.client.BuildLast(owner, name, branch)
		if err != nil {
			log.Printf("Failed to get last build: %v\n", err)
			return
		}
		log.Printf("Restarting last build %s/%s#%d", owner, name, lastBuild.Number)

		build, err := cs.client.BuildStart(owner, name, lastBuild.Number, params)
		if err != nil {
			log.Printf("Failed to start new build: %v\n", err)
			return
		}
		log.Printf("Starting build %s/%s#%d\n", owner, name, build.Number)
	}
}

func FileExists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}
