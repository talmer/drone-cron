Forked from http://github.com/metalmatze/drone-cron
<pre>
-----------------------------------------------

Variables:
  - DRONE_CRON_CONFIG:  Default: "/data/config.yaml"

-----------------------------------------------

Usage:

docker-compose:

drone-cron:
    image: registry.gitlab.com/talmer/drone-cron:latest
    restart: always
    depends_on:
      - drone
      - drone-agent
    volumes:
      - /drone/drone-cron:/data
    environment:
      - DRONE_CRON_CONFIG:  "/data/config.yaml"
     networks:
      - dron-net
</pre>